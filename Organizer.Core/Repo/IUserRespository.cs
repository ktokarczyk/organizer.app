using System;
using System.Threading.Tasks;
using Organizer.Core.Domain;

namespace Organizer.Core.Repo
{
    public interface IUserRespository : IRepository
    {
        Task AddAsync();
        Task<User> GetAsync(Guid userid);
        Task UpdateAsync(User user);
        Task DeleteAsync(Guid userid);
    }
}