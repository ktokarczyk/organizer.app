using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Organizer.Core.Domain;

namespace Organizer.Core.Repo
{
    public interface IEventRepository : IRepository
    {
        Task AddAsync(Event e);
        Task<Event> GetAsync(Guid eventid);
        Task<IEnumerable<Event>> GetByUserIdAsync(Guid eventid);
        Task<IEnumerable<Event>> GetAllAsync();
        Task UpdateAsync(Event e);
        Task DeleteAsync(Guid eventid);
    }
}