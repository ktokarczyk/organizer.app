using System;

namespace Organizer.Core.Domain
{
    public record User
    {
        public Guid UserId { get; init; } 
        public string Username { get; init; } 
        public string Password { get; init; } 
        public string Email { get; init; } 

    }
}