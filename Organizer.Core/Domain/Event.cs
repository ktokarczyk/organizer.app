using System;

namespace Organizer.Core.Domain
{
    public record Event
    {
        public Guid EventId { get; init; } 
        public string Eventname { get; init; } 
        public string Description { get; init; } 
        public DateTimeOffset CreatedAt { get; init; } 
        public DateTimeOffset TimeStart { get; init; } 
        public DateTimeOffset TimeEnd { get; init; } 

    }
}