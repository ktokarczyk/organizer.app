using System;
using System.Threading.Tasks;

namespace Organizer.Infrastructure.Services
{
    public interface IEventService : IService
    {
        Task CreateAsync(
            string eventname,
            string description,
            DateTimeOffset timeStart,
            DateTimeOffset timeEnd
        );
    }
}